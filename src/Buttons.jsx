import React from 'react';

class Button extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          arr: []
      }
      this.getBtnId = this.getBtnId.bind(this);
    }
    getBtnId(event) {
        if(this.state.arr.length === 3){
            this.state.arr.shift()
        }
        this.state.arr.push(event.target.id);
        this.setState({
            arr: this.state.arr
        })
    }
    render() {
      return (
        <div>
            <div>{this.state.arr}</div>
            <button id="1" onClick={this.getBtnId}>1</button>
            <button id="2" onClick={this.getBtnId}>2</button>
            <button id="3" onClick={this.getBtnId}>3</button>
            <button id="4" onClick={this.getBtnId}>4</button>
            <button id="5" onClick={this.getBtnId}>5</button>
        </div>
      );
    }
  }

export default Button;